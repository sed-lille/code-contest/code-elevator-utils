from bottle import route, run, request, response
from elevator import Elevator


@route('/hello')
def hello():
    return "Hello World!\n"


# /call?atFloor=[0-5]&to=[UP|DOWN]
@route('/call')
def call():
    print(request.query['atFloor'], request.query['to'])
    # elevator.call(request.query['atFloor'], request.query['to'])
    return "call"


# go?floorToGo=[0-5]
@route('/go')
def floor_to_go():
    elevator.floor_to_go(request.query['floorToGo'])
    return "go"


@route('/userHasEntered')
def user_has_entered():
    elevator.user_has_entered()
    return "entered"


@route('/userHasExited')
def user_has_exited():
    elevator.user_has_exited()
    return "exited"


# /reset?cause=information+message
@route('/reset')
def reset():
    elevator.user_has_exited()
    return "reset"


@route('/nextCommand')
def next_command():
    return "NOTHING"


elevator = Elevator()
run(host='0.0.0.0', port=8080, debug=True)
