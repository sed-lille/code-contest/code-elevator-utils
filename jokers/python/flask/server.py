from flask import Flask,request

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"

@app.route("/call", methods=['GET'])
def call():
    atFloor = request.args.get('atFloor')
    to = request.args.get('to')
    print(f"call received from {atFloor} to {to}")
    return ""

# TODO: other commands

@app.route("/nextCommand")
def nextCommand():
    # TODO
    return "NOTHING"

if __name__ == "__main__":
    app.run()
