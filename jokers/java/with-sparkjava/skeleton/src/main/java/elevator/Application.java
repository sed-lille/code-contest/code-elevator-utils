package elevator;

import static spark.Spark.*;
import org.apache.log4j.*;

public class Application {
    public static void main(String[] args) {

      BasicConfigurator.configure();
      Logger.getRootLogger().setLevel(Level.INFO);

    	Elevator elevator = new Elevator();
    	get("/", (request, response) -> "Welcome");

    	get("/reset", (request, response) -> {
    		return elevator.reset();
    	});

    	get("/nextCommand", (request, response) -> "NOTHING");
    	get("/call", (request, response) -> "NOTHING");
    	get("/go", (request, response) -> "NOTHING");
    	get("/userHasEntered", (request, response) -> "NOTHING");
    	get("/userHasExited", (request, response) -> "NOTHING");
    }

}
