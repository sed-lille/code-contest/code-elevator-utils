Code contest: Code Elevator
===========================

## 1) The Challenge

**Aim:** Write an elevator engine that is responsible of managing elevator-related events.

Your engine shall implement a public web API.

An external bot will challenge your server. 

The bot is able to challenge several elevator engine servers simultaneously and provide a live ranking.

**Important Note:** Jokers are available in [./jokers](./jokers).
 
## 2) The rules
### The challenger bot
The challenger bot is deployed at [http://code-elevator.lille.inria.fr:8080](http://code-elevator.lille.inria.fr:8080). 
To join in, you must register and provide the following:

- **Login**: whatever you like, prefer your team name or your Inria login
- **Email**: if you have a [gravatar](http://www.gravatar.com/) account (optional), your picture will be displayed automatically.
- **Server url**: the url of your server in the form ``http://<your_ip_address>:<port>``

Example of connection screens:
![connection screen](pics/img_server_register.png)
--
![game status](pics/img_server_logged.png)

### The API
The bot simulates an elevator model and sends event notifications to your server via to the following `HTTP GET` requests:

| URL request                        | Type    | Description                                                                                                                                       |
|------------------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| <code>/call?atFloor=[0-5]&to=[UP&#124;DOWN]</code> | event   | A user at a floor (from 0 to 5) called the elevator to go up(or down)                                                                             |
| `/go?floorToGo=[0-5]`              | event   | A user inside the elevator asks to go to a floor (from 0 to 5)                                                                                    |
| `/userHasEntered`                  | event   | A user has entered the elevator                                                                                                                   |
| `/userHasExited`                   | event   | A user has exited the elevator                                                                                                                    |
| `/reset?cause=information+message` | event   | Something probably went wrong. Reset the whole system                                                                                             |
| `/nextCommand`                     | request | The bot is asking for the next action on the elevator.  Your server must answer one of the following: `NOTHING`, `UP`, `DOWN`, `OPEN` or `CLOSE`. |

Your server shall always return `HTTP 200` status code to acknowledge an event, and the correct answer for the `/nextCommand` request.

 > Note that a visual example of API events is available [here](http://researchers.lille.inria.fr/~dagher/sed-code-contest/).